#!/usr/bin/env python
"""
Generate samples of background from UnDySPuTeD spectra files.
Very similar to `spikes_trainingset_generate.py
"""
from multiprocessing import Pool

import matplotlib
matplotlib.use('agg')
import pandas as pd
import pdb

from pathlib import Path

from spikes_trainingset_generate import generate_sample, load_from_spike_index
root_dir = Path("./")
df = pd.read_pickle(root_dir/"spike_dataset_blanks.pkl")

spike_dirs = [
        Path("/databf/nenufar-tf/ES11/2022/02/20220202_110000_20220202_133000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/ES11/2022/05/20220529_091000_20220529_130000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/05/20230502_091000_20230502_155000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/06/20230601_091000_20230601_135000_SUN_TRACKING/"),
        Path("/databf/nenufar-tf/LT11/2023/07/20230710_101000_20230710_145000_SUN_TRACKING/"),
        ]


aug_len = 100
# lanefiles = list(spike_dir.glob("*spectra"))
for df_ind in df.index:
    file_ind = 0
    while not df['time_at_maximum_intensity'][df_ind][:10].replace('-','') \
        in spike_dirs[file_ind].as_posix():
        file_ind += 1
    lanefiles = list(spike_dirs[file_ind].glob("*spectra"))
    result, resultV = load_from_spike_index(df, lanefiles, df_ind)
    output_dir = Path("/data/mpearse/spike_dataset/blank")
    def parallel_aug(aug):
        return generate_sample(df, df_ind, result, resultV, aug, output_dir, plot=False, blank=True) 
                
    with Pool() as pool:
        pool.map(parallel_aug, range(aug_len))